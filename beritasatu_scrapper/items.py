# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class BeritasatuScrapItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    url = scrapy.Field()
    title = scrapy.Field()
    day_name = scrapy.Field()
    day = scrapy.Field(serializer=int)
    month = scrapy.Field(serializer=int)
    year = scrapy.Field(serializer=int)
    hour = scrapy.Field(serializer=int)
    minute = scrapy.Field(serializer=int)
    location = scrapy.Field()
    content = scrapy.Field()
