# This package will contain the spiders of your Scrapy project
#
# Please refer to the documentation for information on how to create and manage
# your spiders.
import re
from scrapy.spiders import CrawlSpider
from beritasatu_scrapper.utils import parse_date_time

class BeritasatuSpider(CrawlSpider):
    name = "beritasatu"
    allowed_domains = ["beritasatu.com"]
    start_urls = [
        "https://www.beritasatu.com/newsindex/bisnis",
        "https://www.beritasatu.com/newsindex/pasar-modal",
        "https://www.beritasatu.com/newsindex/bank",
        "https://www.beritasatu.com/newsindex/properti",
        "https://www.beritasatu.com/newsindex/keuangan",
    ]

    def parse(self, response):
        for article_page in response.css("div.media-body > a"):
            yield response.follow(article_page, self.parse_article)
        yield response.follow(response.css("ul.pagination > li.next > a::attr(href)").get(), self.parse)

    def parse_article(self, response):
        response_dict = {}
        response_dict["url"] = response.request.url
        response_dict["title"] = response.css("h1.big-headline::text").get()
        response_dict.update(parse_date_time(response.css("span.hz_date_post::text").get()))
        content = ""
        for paragraph in response.css("div.hz_content > p").getall():
            if "Baca Juga:" in paragraph:
                continue
            paragraph = re.sub(r'(<a href=")(.*?)(" target="_blank">)', "", paragraph).replace("</a>", "")
            paragraph = paragraph.replace("<p>", "").replace("</p>", "")
            paragraph = paragraph.replace("<strong>", "").replace("</strong>", "")
            paragraph = paragraph.replace("<em>", "").replace("</em>", "")
            paragraph = paragraph.replace("\u00a0", " ")
            content += paragraph + "\n"
        splitted_content = content.split("\u2013")
        if not splitted_content[0].strip():
            splitted_content = splitted_content[1].strip().split(" - ")
        elif len(splitted_content) < 2:
            splitted_content = splitted_content[0].strip().split(" - ")
        response_dict["location"] = splitted_content[0].replace(", Beritasatu.com", "").strip()
        response_dict["content"] = "".join(splitted_content[1:]).strip()
        return response_dict
