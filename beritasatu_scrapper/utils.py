def parse_date_time(datetime_str):
    date_time = datetime_str.split(" | ")
    date_time[0] = date_time[0].split(" ")
    date_time[0][0] = date_time[0][0].replace(",","")
    date_time[1] = date_time[1].replace(" WIB", "").split(":")
    months = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"]
    return {
        "day_name": date_time[0][0],
        "day": int(date_time[0][1]),
        "month": months.index(date_time[0][2]) + 1,
        "year": int(date_time[0][3]),
        "hour": int(date_time[1][0]),
        "minute": int(date_time[1][1])
    }
